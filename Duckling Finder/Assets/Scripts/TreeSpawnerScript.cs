﻿using UnityEngine;
using System.Collections;

public class TreeSpawnerScript : MonoBehaviour {

	public GameObject treePrefab;
	private int minTreesPerScreen = 5;
	private int maxTreesPerScreen = 12;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void populateFieldWithTrees(float xPos, float yPos, float width, float height) {
		int numTrees = Random.Range (minTreesPerScreen, maxTreesPerScreen);
		Vector3 treePos;
		for (int i = 0; i < numTrees; i++) {
			treePos = new Vector3 (Random.Range(xPos, xPos + width), Random.Range(yPos, yPos - height), treePrefab.transform.position.z);
			treePrefab = ObjectPoolerScript.current.getPooledObject ();
			treePrefab.transform.position = treePos;
			// set sorting order of the trees so that the further down they are, the later in the drawing order they are
			int sortingOrder = -(int)(Mathf.Round(treePos.y * 100f));
			treePrefab.GetComponent<SpriteRenderer> ().sortingOrder = sortingOrder;
			// Grab a prefab from the object pool and set its renderer to be true
			treePrefab.SetActive (true);
			treePrefab.GetComponent<Renderer>().enabled = true;
//			Instantiate (treePrefab, treePos, Quaternion.identity);
		}
	}
}
