﻿using UnityEngine;
using System.Collections;

public class FieldScript : MonoBehaviour {

	private GameObject treeSpawner;
	private GameObject ducklingSpawner;
	private Transform cameraTransform;
	private float spriteHeight;
	private float spriteWidth;

	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.transform;
		SpriteRenderer spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
		spriteHeight = spriteRenderer.sprite.bounds.size.y;
		spriteWidth = spriteRenderer.sprite.bounds.size.x;
		gameObject.GetComponent<SpriteRenderer> ().sortingOrder = -32768;
	}
	
	// Update is called once per frame
	void Update () {
		// TODO: Not a great way to do this. If tags ever change then the only way to figure out why is by runtime debugging. 
		// if I do a public gameobject then I can force compile time errors, just fyi
		treeSpawner = GameObject.FindWithTag ("TreeSpawner");
		ducklingSpawner = GameObject.FindWithTag ("DucklingSpawner");
		if ((transform.position.y) > cameraTransform.position.y + spriteHeight/2) {
			Vector3 newPos = transform.position;
			newPos.y -= 2.0f * spriteHeight;
			treeSpawner.GetComponent<TreeSpawnerScript>().populateFieldWithTrees (newPos.x - spriteWidth/2f, newPos.y + spriteHeight*.5f, spriteWidth, spriteHeight);
			ducklingSpawner.GetComponent<DucklingSpawnerScript>().populateDuckling (newPos.x - spriteWidth/2f, newPos.y + spriteHeight*.5f, spriteWidth, spriteHeight);
			transform.position = newPos;
		}
		else if( (transform.position.x + spriteWidth) < cameraTransform.position.x) {
			Vector3 newPos = transform.position;
			newPos.x += 2.0f * spriteWidth;
			treeSpawner.GetComponent<TreeSpawnerScript>().populateFieldWithTrees (newPos.x - spriteWidth*.4f, newPos.y + spriteHeight*.5f, spriteWidth, spriteHeight);
			ducklingSpawner.GetComponent<DucklingSpawnerScript>().populateDuckling (newPos.x - spriteWidth*.4f, newPos.y + spriteHeight*.5f, spriteWidth, spriteHeight);
			transform.position = newPos;
		}
		else if( (transform.position.x - spriteWidth) > cameraTransform.position.x) {
			Vector3 newPos = transform.position;
			newPos.x -= 2.0f * spriteWidth; 
			treeSpawner.GetComponent<TreeSpawnerScript>().populateFieldWithTrees (newPos.x - spriteWidth*.6f, newPos.y + spriteHeight * .5f, spriteWidth, spriteHeight);
			ducklingSpawner.GetComponent<DucklingSpawnerScript>().populateDuckling (newPos.x - spriteWidth*.6f, newPos.y + spriteHeight * .5f, spriteWidth, spriteHeight);
			transform.position = newPos;
		}
	}
}
