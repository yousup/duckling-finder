﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	private float speed = 0f;
	// Default duckPos is straight down, or 2
	private string duckPos = "2";
	private Vector3 newPosition;

	// Use this for initialization
	void Start () {
		newPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		// TODO: make the camera move relative to Duck
		if (duckPos == "1") {
			newPosition.y -= Time.deltaTime * speed;
			newPosition.x = transform.position.x;
			transform.position = newPosition;
		} else if (duckPos == "2") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 45f));
			newPosition.x += Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 45f));
			transform.position = newPosition;
		} else if (duckPos == "3") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 45f));
			newPosition.x -= Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 45f));
			transform.position = newPosition;
		} else if (duckPos == "4") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 22f));
			newPosition.x += Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 22f));
			transform.position = newPosition;
		} else if (duckPos == "6") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 22f));
			newPosition.x -= Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 22f));
			transform.position = newPosition;
		} else {
			newPosition.y = transform.position.y;
			newPosition.x = transform.position.x;
			transform.position = newPosition;
		}
	}

	public void ChangeCameraSpeed(float speed, string currentSprite) {
		this.speed = speed;
		duckPos = currentSprite;
	}
}
