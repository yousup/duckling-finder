﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DucklingController : MonoBehaviour {

	// The sprites
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	public Sprite sprite4;
	public Sprite sprite5;
	public Sprite sprite6;
	public Sprite sprite7;

	[SerializeField]
	private Collider2D[] colliders;
	private int currentColliderIndex = 0;
	private Scrollbar scrollbar;
	private SpriteRenderer spriteRenderer;
	private Camera camera;

	private readonly float SPEED1 = 0f;
	private readonly float SPEED2 = 3f;
	private readonly float SPEED3 = 6f;
	private readonly float SPEED4 = 9f;
	private readonly string[] spriteTags = {"1", "2", "3", "4", "5", "6", "7"};

	private Vector3 newPosition;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<SpriteRenderer> ().sortingLayerName = "Duckling";
	}
	
	// Update is called once per frame
	void Update () {
		// gameObject.GetComponent<SpriteRenderer> ().sortingOrder = -(int) (Mathf.Round(gameObject.transform.position.y * 100f)+1f);
	}
}
