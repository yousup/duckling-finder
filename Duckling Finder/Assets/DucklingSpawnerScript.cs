﻿using UnityEngine;
using System.Collections;

public class DucklingSpawnerScript : MonoBehaviour {

	public GameObject ducklingPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void populateDuckling(float xPos, float yPos, float width, float height) {
		int random = Random.Range (0, 2);
		// 50% chance of a duckling spawning on a screen
		if (random == 0) {
			Vector3 ducklingPos = new Vector3 (Random.Range (xPos, xPos + width), Random.Range (yPos, yPos - height), ducklingPrefab.transform.position.z);
			Instantiate (ducklingPrefab, ducklingPos, Quaternion.identity);
		}
	}
}
