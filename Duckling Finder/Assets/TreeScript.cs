﻿using UnityEngine;
using System.Collections;

public class TreeScript : MonoBehaviour {

	private Transform cameraTransform;

	// Use this for initialization
	void Start () {
		cameraTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		// If the gameobject is far enough up, set it to inactive and disable its renderer
		if (gameObject.transform.position.y > cameraTransform.position.y + Camera.main.orthographicSize*2) {
			gameObject.GetComponent<Renderer>().enabled = false;
			gameObject.SetActive(false);
		}
	}
}
