﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DuckController : MonoBehaviour {

	// The sprites
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	public Sprite sprite4;
	public Sprite sprite5;
	public Sprite sprite6;
	public Sprite sprite7;

	[SerializeField]
	private Collider2D[] colliders;
	private int currentColliderIndex = 0;
	private Scrollbar scrollbar;
	private SpriteRenderer spriteRenderer;
	private Camera camera;

	private readonly float SPEED1 = 0f;
	private readonly float SPEED2 = 3f;
	private readonly float SPEED3 = 6f;
	private readonly float SPEED4 = 9f;
	private readonly string[] spriteTags = {"1", "2", "3", "4", "5", "6", "7"};

	private Vector3 newPosition;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		camera = Camera.main;
		newPosition = transform.position;
		gameObject.GetComponent<Animator> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<SpriteRenderer> ().sortingOrder = -(int) Mathf.Round(gameObject.transform.position.y * 100f);
		ChangeDuckPositionIfNecessary ();
	}

	private void ChangeDuckPositionIfNecessary() {
		scrollbar = GameObject.FindWithTag ("Scrollbar").GetComponent<Scrollbar>();
		float speed;
		string currentSprite;
		if (scrollbar.value < 0.143f) {
			spriteRenderer.sprite = sprite7;
			currentSprite = spriteTags [6];
			SetColliderForSprite (6);
			speed = SPEED1;
		} else if (scrollbar.value >= 0.143f && scrollbar.value < 0.286f) {
			spriteRenderer.sprite = sprite6;
			currentSprite = spriteTags [5];
			SetColliderForSprite (5);
			speed = SPEED2;
		} else if (scrollbar.value >= 0.286f && scrollbar.value < 0.429f) {
			spriteRenderer.sprite = sprite3;
			currentSprite = spriteTags [2];
			SetColliderForSprite (2);
			speed = SPEED3;
		} else if (scrollbar.value >= 0.429f && scrollbar.value < 0.571f) {
			spriteRenderer.sprite = sprite1;
			currentSprite = spriteTags [0];
			SetColliderForSprite (0);
			speed = SPEED4;
		} else if (scrollbar.value >= 0.571f && scrollbar.value < 0.714f) {
			spriteRenderer.sprite = sprite2;
			currentSprite = spriteTags [1];
			SetColliderForSprite (1);
			speed = SPEED3;
		} else if (scrollbar.value >= 0.714f && scrollbar.value < 0.857f) {
			spriteRenderer.sprite = sprite4;
			currentSprite = spriteTags [3];
			SetColliderForSprite (3);
			speed = SPEED2;
		} else {
			spriteRenderer.sprite = sprite5;
			currentSprite = spriteTags [4];
			SetColliderForSprite (4);
			speed = SPEED1;
		}

		// Change Duck's speed
		if (currentSprite == "1") {
			newPosition.y -= Time.deltaTime * speed;
			newPosition.x = transform.position.x;
			transform.position = newPosition;
		} else if (currentSprite == "2") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 45f));
			newPosition.x += Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 45f));
			transform.position = newPosition;
		} else if (currentSprite == "3") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 45f));
			newPosition.x -= Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 45f));
			transform.position = newPosition;
		} else if (currentSprite == "4") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 22f));
			newPosition.x += Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 22f));
			transform.position = newPosition;
		} else if (currentSprite == "6") {
			newPosition.y -= Time.deltaTime * (speed * Mathf.Sin (Mathf.Deg2Rad * 22f));
			newPosition.x -= Time.deltaTime * (speed * Mathf.Cos (Mathf.Deg2Rad * 22f));
			transform.position = newPosition;
		} else {
			newPosition.y = transform.position.y;
			newPosition.x = transform.position.x;
			transform.position = newPosition;
		}

		// Change camera speed
		camera.GetComponent<CameraController> ().ChangeCameraSpeed (speed, currentSprite);
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		Debug.Log ("Hit " + other.gameObject);
		gameObject.GetComponent<Animator> ().enabled = true;
		gameObject.GetComponent<Animator> ().SetBool ("hitTree", true);
	}

	public void SetColliderForSprite( int spriteNum )
	{
		colliders[currentColliderIndex].enabled = false;
		currentColliderIndex = spriteNum;
		colliders[currentColliderIndex].enabled = true;
	}

}
